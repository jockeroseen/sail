export default {
  responsive: true,
  hoverMode: 'label',
  stacked: false,
  title: {
    display: false,
  },
  animation: {
    duration: 1000,
  },
  legend: {
    display: true,
    position: 'bottom',
    labels: {
      fontColor: '#fff',
      fontStyle: 'bold',
    },
  },
  ticks: {
    fontColor: '#fff',
  },
  tooltips: {
    backgroundColor: 'rgba(255,255,255, .85)',
    titleFontColor: 'rgb(77, 18, 123)',
    titleFontSize: 15,
    titleSpacing: 10,
    titleMarginBottom: 10,
    bodySpacing: 5,
    bodyFontSize: 14,
    bodyFontColor: '#2b3452',
    xPadding: 10,
    yPadding: 10,
    borderWidth: 3,
    displayColors: false,
    callbacks: {
      label: (item) => {
        if (item.datasetIndex === 0) {
          return `${item.yLabel} sessions`;
        }
        return `${Math.round(item.yLabel)}% direct traffic`;
      },
    },
  },
  scales: {
    xAxes: [{
      display: true,
      gridLines: {
        offsetGridLines: false,
        color: 'rgba(255,255,255, .2)',
      },
      ticks: {
        fontColor: '#FFF',
        fontStyle: 'bold',
      },
    }],
    yAxes: [{
      type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
      display: true,
      position: 'left',
      id: 'visitors',
      scaleLabel: {
        display: false,
      },
      ticks: {
        fontColor: '#FFF',
        fontStyle: 'bold',
      },
      // grid line settings
      gridLines: {
        color: 'rgba(255,255,255, .2)', // only want the grid lines for one axis to show up
        display: true,
      },
    }, {
      type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
      display: true,
      position: 'right',
      id: 'direct',
      scaleLabel: {
        display: false,
      },
      ticks: {
        min: 0,
        max: 100,
        fontColor: '#FFF',
        fontStyle: 'bold',
        callback: value => `${parseInt(value, 10)}%`,
      },
      // grid line settings
      gridLines: {
        display: false,
      },
    }],
  },
};
