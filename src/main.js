/* eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import GoogleAuth from 'vue-google-auth';
import moment from 'moment';
import App from './App';

Vue.prototype.moment = moment;
Vue.config.productionTip = false;
Vue.use(GoogleAuth, { client_id: '439873569399-bsqj0vagrfuk10v71e1pf9s96jogfc8d.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/analytics.readonly' });
Vue.googleAuth().load();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
});
