export default {
  fetch(view, startDate, endDate) {
    return new Promise((resolve, reject) => {
      window.gapi.client.request({
        path: '/v4/reports:batchGet',
        root: 'https://analyticsreporting.googleapis.com/',
        method: 'POST',
        body: {
          reportRequests: [
            {
              viewId: view,
              dateRanges: [
                {
                  startDate: startDate.format('YYYY-MM-DD'),
                  endDate: endDate.format('YYYY-MM-DD'),
                },
              ],
              metrics: [
                {
                  expression: 'ga:sessions',
                },
              ],
              dimensions: [
                {
                  name: 'ga:date',
                },
              ],
              pivots: [
                {
                  metrics: [
                    {
                      expression: 'ga:sessions',
                    },
                  ],
                  dimensions: [
                    {
                      name: 'ga:sourceMedium',
                    },
                  ],
                  dimensionFilterClauses: [
                    {
                      filters: [
                        {
                          dimensionName: 'ga:sourceMedium',
                          operator: 'EXACT',
                          expressions: '(direct) / (none)',
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
      }).then((res) => {
        resolve(res.result.reports);
      }, error => reject(error));
    });
  },
};
